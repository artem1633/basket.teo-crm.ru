<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.min.css',
        'css/theme-default.css',
        'css/site.css',
        'css/style.css',
        'css/screen.css',
        //'css/adminlte.css',
    ];
    public $js = [
        //'js/all.min.js',
        //'js/common.js',
        //'js/main.js',
        //'js/sparklines.js',
        //'js/plugins/jquery/jquery.min.js',
        'js/bootstrap.min.js',
        'js/plugins/jquery/jquery-ui.min.js',
        //'js/plugins/bootstrap/bootstrap.min.js',
        'js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js',
        'js/plugins.js',
        'js/actions.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
