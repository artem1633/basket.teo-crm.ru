<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

class YandexMapAsset extends AssetBundle
{
    public $css = [
        'css/bootstrap.min.css',
    ];
    public $js = [
        'http://api-maps.yandex.ru/2.1/?load=package.full&lang=ru-RU'
        
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}