<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Company;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) { 
            if (Yii::$app->user->identity->status==9) {
                //если суперадмин то в модуль заказов админки
                return $this->redirect(['/admin/orders/index']);    
            } else {
                //иначе в кабинет заказов компании
                return $this->redirect(['/orders/index']);    
            }
            
        }else
        {
            return $this->redirect(['site/login']);
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */

    public function actionOnline()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $company = Company::find()->where(['id'=>Yii::$app->user->identity->company_id])->one() ;
            if ($company->online == 0) {
                $company->online = 1;
                $company->save();
                return ['checked' => true,'q'=>$company->name];
            } else {
                $company->online = 0;
                $company->save();
                return ['checked' => false,'q'=>$company->name];
            }
        }
    }
    
    public function actionGetstatus()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $company = Company::find()->where(['id'=>Yii::$app->user->identity->company_id])->one() ;
                return ['status' => $company->online];
        }
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }
    

    
    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        $this->redirect(['login']);
    }


    public function actionPolicy()
    {
        echo "string";
        die;
        return $this->render('index', [
        ]);  
    }

    public function actionMenuPosition()
    {
        $session = Yii::$app->session;
        if($session['body'] == null | $session['body'] == 'small') $session['body'] = 'large';
        else $session['body'] = 'small';

        if($session['left'] == null | $session['left'] == 'small') $session['left'] = 'large';
        else $session['left'] = 'small';

    }
}
