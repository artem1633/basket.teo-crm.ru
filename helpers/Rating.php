<?php

namespace app\helpers;

use Yii;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class Rating
{
    
    public static function Rate($rating)
    {
        $rate='';
        for ($i=1;$i<=$rating;$i++) {
            $rate.='<span class="glyphicon glyphicon-star" ></span>';
        }
        for ($i=$rating;$i<5;$i++) {
            $rate.='<span class="glyphicon glyphicon-star-empty" ></span>';
        }
        return $rate;
    }
    
}
