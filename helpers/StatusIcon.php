<?php

namespace app\helpers;

use Yii;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class StatusIcon
{
    
    public static function StatusList()
    {
        return [1 => 'Да',
                0 => 'Нет'];
    }
    
    public static function StatusIcon($status)
    {
        return $status ? '<span style="color:green" class = "glyphicon glyphicon-ok"></span>' :'<span style="color:red" class = "glyphicon glyphicon-remove"></span>';
    }
    
}
