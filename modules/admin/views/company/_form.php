<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use app\models\CategoryCompany;

/* @var $this yii\web\View */
/* @var $model app\models\Company */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-form">

    <?php $form = ActiveForm::begin([
          'options'=>['enctype'=>'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'category_company_id')->dropDownList(CategoryCompany::getAllCategoryCompany()) ?>
            <?= $form->field($model, 'online')->checkbox() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'files')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*','multiple'=>false],
                'pluginOptions' => [
                    'initialPreview'=>'/uploads/company/'.$model->picture,
                    'initialPreviewAsData'=>true,
                    'overwriteInitial'=>TRUE,
                    'showPreview' => true,
                    'showCaption' => true,
                    'showRemove' => true,
                    'showUpload' => false,
                    'browseLabel' => '',
                    'removeLabel' => '',
                ]
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'coord_x')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'coord_y')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'percent_order')->textInput(['maxlength' => true]) ?>
        </div>

    </div>
    

    

    

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
