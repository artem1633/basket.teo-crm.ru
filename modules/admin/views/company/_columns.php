<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\helpers\StatusIcon;
use kartik\grid\GridView;
use app\models\CategoryCompany;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'description',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'picture',
        'format' => 'raw',
        'value' => 
        function ($model) {   
            if ($model->picture!='')
                return '<img src="/uploads/company/'.$model->picture.'" width="80px" height="auto">'; else return 'no image';
            },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'category_company_id',
        'filter' => CategoryCompany::getAllCategoryCompany(),
        'value' => 'categoryCompany.name'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'coord_x',
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'coord_y',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'percent_order',
     ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'online',
         'format'=>"raw",
         'filter'=> StatusIcon::StatusList(),
         'value'=>function ($data) {
            return StatusIcon::StatusIcon($data->online);
         }
     ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
//                'template'=>'{view} {update} {delete} {info}',
//                'buttons' => [
//        'info' => function ($url, $model, $key) {
//            return Html::a('icon', Url::current(['online', 'id' => $key]),
//                    ['role'=>'modal-remote', 'title'=>'Просмотр','data-method'=>false,// for overide yii data api
//                          'data-request-method'=>'post',]);
//        }],
//                
        'width' => '120px',
        'viewOptions'=>['label'=>'<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>','role'=>'modal-remote'],
        'updateOptions'=>['label'=>'<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>','role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'label'=>'<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверенны?',
                          'data-confirm-message'=>'Вы действительно хотите удалить запись'],  
    ],

];   