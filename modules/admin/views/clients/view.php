<?php

use yii\widgets\DetailView;
use app\models\Clients;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */
?>
<div class="clients-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'phone',
            'car_type',
            'car_model',
            'car_number',
            'car_color',
            [
                'class'=>'\kartik\grid\DataColumn',
                'attribute'=>'status',
                'filter'=> Clients::getStatuses(),
                'value' => function ($data) {
                    return Clients::getNameStatus($data['status']);
                 }
            ],
            'coord_x',
            'coord_y',
        ],
    ]) ?>

</div>