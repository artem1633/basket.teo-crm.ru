<?php
use yii\helpers\Url;
use app\models\Clients;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'phone',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'car_type',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'car_model',
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'car_number',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'car_color',
     ],
      [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'status',
         'filter'=> Clients::getStatuses(),
         'value' => function ($data) {
            return Clients::getNameStatus($data['status']);

         }
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'coord_x',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'coord_y',
     ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'width' => '120px',
        'viewOptions'=>['label'=>'<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>','role'=>'modal-remote'],
        'updateOptions'=>['label'=>'<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>','role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'label'=>'<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверенны?',
                          'data-confirm-message'=>'Вы действительно хотите удалить запись'], 
    ],

];
