<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
?>
<div class="product-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'company_id',
            'name',
            'description',
            'weight',
            'price',
            'time_prepare:datetime',
            'valid',
            'status',
            'category_product_id',
        ],
    ]) ?>
    <div class="row">
        <div class="col-md-12">
    <?php echo '<img src="/uploads/'.$model->picture.'" width="200px" height="auto">'; ?>
        </div>
    </div>
    

</div>
