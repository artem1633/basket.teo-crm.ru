<?php

use yii\helpers\Html;
use dosamigos\chartjs\ChartJs;
use yii\helpers\ArrayHelper;


$this->title = 'Рабочий стол';
?>
<div class="site-index">
    <div class="row">
        <div class="col-md-3 col-xs-12 col-sm-6">
            <div class="widget widget-warning widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-file"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count"><?=$ordersAll?></div>
                    <div class="widget-title">Всего заказов</div>
                    
                </div>
                <div class="widget-controls">                                
                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Удалить виджет"><span class="fa fa-times"></span></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-xs-12 col-sm-6">
            <div class="widget widget-warning widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-files-o"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count"><?=$ordersToday?></div>
                    <div class="widget-title">Сегодня заказов</div>
                </div>
                <div class="widget-controls">                                
                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Удалить виджет"><span class="fa fa-times"></span></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-xs-12 col-sm-6">
            <div class="widget widget-warning widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-envelope"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count"><?=$ordersSumma?></div>
                    <div class="widget-title">Сумма заказов всего</div>
                </div>
                <div class="widget-controls">                                
                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Удалить виджет"><span class="fa fa-times"></span></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-xs-12 col-sm-6">
            <div class="widget widget-warning widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-send"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count"><?=$ordersTodaySumma?></div>
                    <div class="widget-title">Сумма заказов сегодня</div>
                </div>
                <div class="widget-controls">                                
                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Удалить виджет"><span class="fa fa-times"></span></a>
                </div>
            </div>
        </div>

    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success panel-hidden-controls">
                <div class="panel-heading ui-draggable-handle">
                    <h3 class="panel-title">Информация за месяц количество заказов</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                                <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                            </ul>                                        
                        </li>
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>                                
                </div>
                <div class="panel-body">
                    
                    <div class="col-md-12">
     <?= ChartJs::widget([
                            'type' => 'line',
                            'id' => 'lines',
                            'options' => [
                                'class' => 'chartjs-render-monitor',
                                'height' => 80,
                                'width' => 300
                            ],
                            'data' => [
                                'labels' => ArrayHelper::getColumn($orderByMounth, 'd'),
                                'datasets' => [
                                    [
                                        'label' => "Количестро заказов",
                                        'backgroundColor' => "rgba(255,99,132,0.2)",
                                        'borderColor' => "rgba(255,99,132,1)",
                                        'pointBackgroundColor' => "rgba(255,99,132,1)",
                                        'pointBorderColor' => "#fff",
                                        'pointHoverBackgroundColor' => "#fff",
                                        'pointHoverBorderColor' => "rgba(255,99,132,1)",
                                        'data' => ArrayHelper::getColumn($orderByMounth, 'c'),
                                    ]
                                ]
                            ]
                        ]);
                        ?>
                    </div>

                </div>      
                <div class="panel-footer">
                </div>                            
            </div>
        </div>
    </div>

</div>


<!-- <div class="col-md-12">
    <div>
        <canvas id="myChart" width="300" height="80" ></canvas>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.min.js"></script> -->
<?php 
/*$this->registerJs(<<<JS

var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
    datasets: [{
      label: 'apples',
      data: [12, 19, 3, 17, 6, 3, 7],
      backgroundColor: "rgba(153,255,51,0.4)"
    }, {
      label: 'oranges',
      data: [2, 29, 5, 5, 2, 3, 10],
      backgroundColor: "rgba(255,153,0,0.4)"
    }]
  }
});

JS
);*/
?>