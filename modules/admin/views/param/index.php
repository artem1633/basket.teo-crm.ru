<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dosamigos\tinymce\TinyMce;


/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Параметры';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="product-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'login_SMS')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'password_SMS')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'API_SMS')->textInput(['maxlength' => true]) ?>
             <?= $form->field($model, 'key_push')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'API_telegram')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'proxy')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'text_SMS')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'MERCHANT_PUBLIC_ID')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'MERCHANT_API_PASS')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php echo $form->field($model, 'about')->widget(TinyMce::className(), [
                    'options' => ['rows' => 6],
                    'language' => 'ru',
                    'clientOptions' => [
                        'plugins' => [
                            'advlist autolink lists link charmap  print hr preview pagebreak',
                            'searchreplace wordcount textcolor visualblocks visualchars code fullscreen nonbreaking',
                            'save insertdatetime media table contextmenu template paste image'
                        ],
                        'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
                    ]
                ]);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php echo $form->field($model, 'support')->widget(TinyMce::className(), [
                    'options' => ['rows' => 6],
                    'language' => 'ru',
                    'clientOptions' => [
                        'plugins' => [
                            'advlist autolink lists link charmap  print hr preview pagebreak',
                            'searchreplace wordcount textcolor visualblocks visualchars code fullscreen nonbreaking',
                            'save insertdatetime media table contextmenu template paste image'
                        ],
                        'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
                    ]
                ]);
            ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
    
</div>
