<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\helpers\Url;
use app\models\Users;
use yii\widgets\Pjax;


$path = 'http://' . $_SERVER['SERVER_NAME'].'/examples/images/users/avatar.jpg';

 
?>
<?php 
        $session = Yii::$app->session;
        if($session['left'] == null | $session['left'] == 'small') $left="x-navigation-custom x-navigation-minimized";
        else $left="x-navigation-custom";
    ?>
<!-- START PAGE SIDEBAR -->
            <div class="page-sidebar page-sidebar-fixed scroll">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation <?=$left?>">
                    <li class="xn-logo">
                        <a href="<?=Url::toRoute([Yii::$app->homeUrl])?>"><?=Yii::$app->name?></a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="<?=$path?>" alt="John Doe"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="<?=$path?>" alt="John Doe"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name"><?=Yii::$app->user->identity->fullname?></div>
                                <div class="profile-data-title"><?php if (!Yii::$app->user->isGuest) echo Yii::$app->user->identity->getCurrentRole()?></div>
                            </div>
                        </div>                                                                        
                    </li>
                    
                    <li class="xn-title">Menu</li>
                    
                    <li <?php if (Yii::$app->controller->id=='default'){ echo ' class = "active" ';}?>>
                        <?= Html::a('<span class="fa fa-desktop"></span> <span class="xn-text">Показатели</span>', ['/admin/default/index'], []); ?>
                    </li> 
                    <li <?php if (Yii::$app->controller->id=='orders'){ echo ' class = "active" ';}?>>
                        <?= Html::a('<span class="glyphicon glyphicon-euro"></span> <span class="xn-text">Заказы</span>', ['/admin/orders/index'], []); ?>
                    </li> 
                    <li <?php if (Yii::$app->controller->id=='product'){ echo 'class="active"';}?>>
                        <?= Html::a('<span class="glyphicon glyphicon-shopping-cart"></span> <span class="xn-text">Товары</span>', ['/admin/product/index'], []); ?>
                    </li> 
                    <li <?php if (Yii::$app->controller->id=='company-rating'){ echo 'class="active"';}?>>
                        <?= Html::a('<span class="glyphicon glyphicon-signal"></span> <span class="xn-text">Отзывы</span>', ['/admin/company-rating/index'], []); ?>
                    </li> 
                    <li class="xn-openable
                        <?php if (Yii::$app->controller->id=='category-company'){ echo " active";}?>
                        <?php if (Yii::$app->controller->id=='category-product'){ echo " active";}?>
                        <?php if (Yii::$app->controller->id=='company'){ echo " active";}?>
                        <?php if (Yii::$app->controller->id=='users'){ echo " active";}?>
                        <?php if (Yii::$app->controller->id=='clients'){ echo " active";}?>
                        <?php if (Yii::$app->controller->id=='param'){ echo " active";}?>
                        "><?= Html::a('<span class="fa fa-wrench"></span> <span class="xn-text">Настройки</span>', ['#'], []); ?>
        		<ul>
                            <li <?php if (Yii::$app->controller->id=='company'){ echo 'class="active"';}?>>
                                <?= Html::a('<span class="glyphicon glyphicon-globe"></span> <span >Компании</span>', ['/admin/company/index'], []); ?>
                            </li>
                            <li <?php if (Yii::$app->controller->id=='category-company'){ echo 'class="active"';}?>>
                                <?= Html::a('<span class="glyphicon glyphicon-cutlery"></span> <span >Категории компаний</span>', ['/admin/category-company/index'], []); ?> 
                            </li> 
                            <li <?php if (Yii::$app->controller->id=='category-product'){ echo 'class="active"';}?>>
                                <?= Html::a('<span class="glyphicon glyphicon-ice-lolly-tasted"></span> <span >Категории товаров</span>', ['/admin/category-product/index'], []); ?>
                            </li>
                            <li <?php if (Yii::$app->controller->id=='users'){ echo 'class="active"';}?>>
                                <?= Html::a('<span class="fa fa-users"></span> <span>Пользователи</span>', ['/admin/users/index'], []); ?>
                            </li> 
                            <li <?php if (Yii::$app->controller->id=='clients'){ echo 'class="active"';}?>>
                                <?= Html::a('<span class="fa fa-wrench"></span> <span>Клиенты</span>', ['/admin/clients/index'], []); ?>
                            </li>
                            <li <?php if (Yii::$app->controller->id=='param'){ echo 'class="active"';}?>>
                                <?= Html::a('<span class="fa fa-wrench"></span> <span>Параметры</span>', ['/admin/param/index'], []); ?>
                            </li>
                        </ul>
                    </li>
                    
                    
                                
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->