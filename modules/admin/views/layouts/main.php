<?php
use yii\helpers\Html;

    if (class_exists('backend\assets\AppAsset')) {
        backend\assets\AppAsset::register($this);
    } else {
        app\assets\AppAsset::register($this);
    }

    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <?php 
        $session = Yii::$app->session;
        if($session['body'] == null | $session['body'] == 'small') $body="page-navigation-toggled page-container-wide";
        else $body="";
    ?>
    <body class="">
    <?php $this->beginBody() ?>
        <div class="page-container <?=$body ?>">

            <?= $this->render( 'left.php', [] ) ?>
            <!-- PAGE CONTENT -->
            <div class="page-content">
                <?= $this->render( 'header.php', [] ) ?>
                <?= $this->render( 'content.php', ['content' => $content, ] ) ?>
             </div>                       
            <!-- END PAGE CONTENT -->
        </div>
    <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>

