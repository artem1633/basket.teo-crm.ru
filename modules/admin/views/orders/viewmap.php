<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset; 
use app\models\Orders;


CrudAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $company app\models\Company */
/* @var $client app\models\Clients */
?>

<div id="ajaxCrudDatatable">
    
<div class="orders-view">
    <div class="box box-default" style="margin-top: 30px;margin-bottom: 20px">
        <div class="box-body">
            <div class="row">
                <?php if ($model->status != Orders::ORDER_COMPLETE ) :?>
                <div class="col-md-7">
                    <div id="map">
                    </div>
                </div>     
                <?php endif;?>
                
                <div class="col-md-5">
                    <div class="panel panel-success panel-hidden-controls">
                        <div class="panel-heading ui-draggable-handle">
                            <h1 class="panel-title"> <i class="fa fa-asterisk"> </i> Информация о заказе </h1>
                            <?=Html::a('Редактировать &nbsp;<i class="glyphicon glyphicon-pencil"></i>',['/admin/orders/update','id'=>$model->id],[
                                    'class'=>"btn btn-warning btn-rounded pull-right",
                                    'title'=>"Редактировать",
                                    'role'=>"modal-remote",
                            ])?>
                        </div>    
                        <div class="panel-body">
                            <p><b><?=$model->getAttributeLabel('company_id')?></b> : <?=Html::encode($model->company->name)?></p>
                            <p><b><?=$model->getAttributeLabel('order_date')?></b> : <?=Html::encode($model->order_date)?></p>
                            <p><b>Имя клиента</b> : <?=Html::encode($model->clients->name)?></p>
                            <p><b>Телефон</b> : <?=Html::encode($model->clients->phone)?></p>
                            <hr>
                            <p><b><?=$model->getAttributeLabel('summa')?></b> : <?=Html::encode($model->summa)?></p>
                            <p><b><?=$model->getAttributeLabel('time_last')?></b> : <span id="time-last"></span></p>
                            <p><b><?=$model->getAttributeLabel('distance')?></b> : <span id="distance"></span></p>
                            <p><b><?=$model->getAttributeLabel('time_max')?></b> : <?=Html::encode($model->time_max)?></p>
                        </div>
                    </div>
                    <div class="panel panel-success panel-hidden-controls">
                        <div class="panel-heading ui-draggable-handle">
                            <h1 class="panel-title"> <i class="fa fa-user"> </i> Личная информация </h1>
                            <ul class="panel-controls">
                                <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                            </ul>
                        </div>    
                        <div class="panel-body">
                            <p><b><?=$model->getAttributeLabel('car_type')?></b> : <?=Html::encode($model->car_type)?></p>
                            <p><b><?=$model->getAttributeLabel('car_model')?></b> : <?=Html::encode($model->car_model)?></p>
                            <p><b><?=$model->getAttributeLabel('car_number')?></b> : <?=Html::encode($model->car_number)?></p>
                            <p><b><?=$model->getAttributeLabel('car_color')?></b> : <?=Html::encode($model->car_color)?></p>
                        </div>
                    </div>
                    <div class="panel panel-success panel-hidden-controls">
                        <div class="panel-heading ui-draggable-handle">
                            <h1 class="panel-title"> <i class="fa fa-list"> </i> Информация о товаре </h1>
                            <ul class="panel-controls">
                                <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                            </ul>
                        </div>    
                        <div class="panel-body">
                            <div class=table-responsive">
                                <table class="table table-bordered" >
                                    <thead>
                                        <tr>
                                            <th style="width: 60%">Товар</th>
                                            <th style="width: 20%">К-во</th>
                                            <th style="width: 20%">Сумма</th>        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($model->ordersItems as $item) :?>
                                        <tr>
                                            <td ><?=Html::encode($item->product->name)?></td>
                                            <td ><?=Html::encode($item->amount)?></td>
                                            <td ><?=Html::encode($item->summa)?></td>
                                        </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                </table>  
                            </div>
                        </div>
                    </div>
                </div>        
            </div>
        </div>
    </div>
</div>
</div>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=&lt;ваш API-ключ&gt;" type="text/javascript"></script>
	<style>
         #map {
            width: 100%; height: 600px; padding: 0; margin: 0;
        }
    </style>
    <style>
        .YMaps-layer-container img{
max-width: none;
}
    </style>

<script>
function init () {
    /**
     * Создаем мультимаршрут.
     * Первым аргументом передаем модель либо объект описания модели.
     * Вторым аргументом передаем опции отображения мультимаршрута.
     * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/multiRouter.MultiRoute.xml
     * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/multiRouter.MultiRouteModel.xml
     */
    
    
    var multiRoute = new ymaps.multiRouter.MultiRoute({
        // Описание опорных точек мультимаршрута.
        referencePoints: [
            [<?=$company->coord_x?>,<?=$company->coord_y?>],
            [<?=$client->coord_x?>,<?=$client->coord_y?>],
        ],
        // Параметры маршрутизации.
        params: {
            // Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
            results: 1,
            avoidTrafficJams: true,
        }
    }, {
        // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
        boundsAutoApply: true,
        wayPointStartIconLayout: "default#image",
        wayPointStartIconImageHref: "/img/burger.png",
        wayPointStartIconImageSize: [40, 40],
        wayPointStartIconImageOffset: [-20, -20],
        // Задаем собственную картинку для последней путевой точки.
        wayPointFinishIconLayout: "default#image",
        wayPointFinishIconImageHref: "/img/car.png",
        wayPointFinishIconImageSize: [40, 40],
        wayPointFinishIconImageOffset: [-20, -20],
        
    });

    

    // Создаем карту с добавленными на нее кнопками.
    var myMap = new ymaps.Map('map', {
        center: [55.750625, 37.626],
        zoom: 7,
    }, {
        buttonMaxWidth: 300
    });

    // Добавляем мультимаршрут на карту.
    myMap.geoObjects.add(multiRoute);
    multiRoute.model.events.add("requestsuccess", function (event) {

console.log(multiRoute.getRoutes().get(0).properties.get("distance").value);
$("#time-last").html(multiRoute.getRoutes().get(0).properties.get("duration").text);
$("#distance").html(multiRoute.getRoutes().get(0).properties.get("distance").text);

});
    
}

ymaps.ready(init);
</script>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>