<?php
use yii\helpers\Url;
use app\models\Orders;
use app\models\Company;
use kartik\helpers\Html;



return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'company_id',
        'filter'=> Company::getAllCompany(),
        'value'=>'company.name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'order_date',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'summa',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'car_type',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'car_number',
    ],
    
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'format'=>'datetime',
//         'attribute'=>'time_last',
//     ],
//     [
//         'class'=>'\kartik\grid\DataColumn',
//         'attribute'=>'distance',
//     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'time_max',
     ],
    [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'status',
         'filter'=> Orders::getStatuses(),
         'format'=>'raw',
         'value'=>function ($data) {
            return \kartik\helpers\Html::dropDownList('status'.$data->id, $data->status, Orders::getStatuses(),['id'=>'idstatus'.$data->id, 'onChange'=>'changeStatus('.$data->id.')']);
         }
     ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template' => '{view}{update} {delete}',
        'buttons'=>
            [
                'update' => function ($url, $model, $key) {
                    return $model->status == Orders::ORDER_COMPLETE ? '': Html::a('<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>', $url);
                }
            ],
                
        'width' => '120px',
        'viewOptions'=>['label'=>'<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>','title'=>'Просмотр','target'=>'_blank','data-pjax'=>"0"],
        'updateOptions'=>['label'=>'','role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'label'=>'<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверенны?',
                          'data-confirm-message'=>'Вы действительно хотите удалить запись'], 
    ],

];   