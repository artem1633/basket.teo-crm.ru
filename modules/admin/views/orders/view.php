<?php

use kartik\helpers\Html;



/* @var $this yii\web\View */
/* @var $model app\models\Orders */
?>

<div class="orders-view">
    
    <div class="box box-default" style="margin-top: 30px;margin-bottom: 20px">
    <div class="box-body">
    
       <div class="row">
        <table style="border-style: solid;margin-left: 20px">
            <tr>
            <td>
            <div class="col-md-12">
                <div class="table-responsive">
                    <table style="margin-top: 20px;margin-left: 20px;font-size: 14px">
                        <tbody>
                            <tr>
                                <td style="width: 280px"><?=$model->getAttributeLabel('company_id')?></td>
                                <td ><b><?=Html::encode($model->company->name)?></b></td>
                            </tr>
                            <tr>
                                <td style="width: 280px"><?=$model->getAttributeLabel('order_date')?></td>
                                <td ><b><?=Html::encode($model->order_date)?></b></td>
                            </tr>
                        </tbody>
                    </table><hr>
    
                    <table style="margin-top: 20px;margin-left: 20px;font-size: 14px">
                        <tbody>
                            <tr>
                                <td style="width: 280px"><?=$model->getAttributeLabel('summa')?></td>
                                <td ><b><?=Html::encode($model->summa)?></b></td>
                            </tr>
                            <tr>
                                <td style="width: 280px"><?=$model->getAttributeLabel('time_last')?></td>
                                <td ><b><?=Html::encode($model->time_last)?></b></td>
                            </tr>
                            <tr>
                                <td style="width: 280px"><?=$model->getAttributeLabel('time_max')?></td>
                                <td ><b><?=Html::encode($model->time_max)?></b></td>
                            </tr>
                        </tbody>
                    </table><hr>
                    <h3 style="color: #1b78b6"><i class="fa fa-user"> </i> <b> Личная информация</b></h3>
                    <table style="margin-top: 20px;margin-left: 20px;font-size: 14px">
                        <tbody>
                            <tr>
                                <td style="width: 280px"><?=$model->getAttributeLabel('car_type')?></td>
                                <td ><b><?=Html::encode($model->car_type)?></b></td>
                            </tr>
                            <tr>
                                <td style="width: 280px"><?=$model->getAttributeLabel('car_model')?></td>
                                <td ><b><?=Html::encode($model->car_model)?></b></td>
                            </tr>
                            <tr>
                                <td style="width: 280px"><?=$model->getAttributeLabel('car_number')?></td>
                                <td ><b><?=Html::encode($model->car_number)?></b></td>
                            </tr>
                            <tr>
                                <td style="width: 280px"><?=$model->getAttributeLabel('car_color')?></td>
                                <td ><b><?=Html::encode($model->car_model)?></b></td>
                            </tr>
                        </tbody>
                    </table><hr>
                    <h3 style="color: #1b78b6"><i class="glyphicon glyphicon-ice-lolly-tasted"> </i> <b> Информация о товаре</b></h3>
                    <table class="table table-bordered" style="margin-top: 20px;margin-left: 20px;font-size: 14px">
                        <thead>
                            <tr>
                                <th style="width: 60%">Товар</th>
                                <th style="width: 20%">К-во</th>
                                <th style="width: 20%">Сумма</th>        
                            </tr>
                        </thead>
                        
                        <tbody>
                            <?php foreach ($model->ordersItems as $item) :?>
                            <tr>
                                
                                <td ><?=Html::encode($item->product->name)?></td>
                                <td ><?=Html::encode($item->amount)?></td>
                                <td ><?=Html::encode($item->summa)?></td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>

                    <br>
                </div>
            </div>
            </td>
            </tr>
            </table>
       </div>
           

           
    </div>
</div>
</div>