<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;
use kartik\select2\Select2;
use kartik\datetime\DateTimePicker;
use app\models\Orders;
use app\models\Product;
use app\models\Company;
use app\models\Clients;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'company_id')->dropDownList(Company::getAllCompany()) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'order_date')->widget(DateTimePicker::classname(), [
	'options' => ['placeholder' => 'Enter event time ...'],
	'pluginOptions' => [
		'autoclose' => true
	]
]);?>
        </div>
        
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'status')->dropDownList(Orders::getStatuses()) ?>
        </div>
        <div class="col-md-6">
             <?= $form->field($model, 'client_id')->dropDownList(Clients::getAllClients()) ?>
        </div>
        
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'car_type')->textInput() ?>
        </div>
        <div class="col-md-6">
             <?= $form->field($model, 'car_model')->textInput() ?>
        </div>
        
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'car_number')->textInput() ?>
        </div>
        <div class="col-md-6">
             <?= $form->field($model, 'car_color')->textInput() ?>
        </div>
        
    </div>

<div class="col-md-12" style="background: #fff">
                <h1>Товари</h1>
                <div class="row" style="margin-top: 20px;background: #ecf0f5">
                    <div class="col-md-12">
                        <?php echo $form->field($model, 'ordersItems')->widget(MultipleInput::className(), [
                                'class' =>'btn btn-success',
                                'columns' => [
                                    [
                                        'name'  => 'product_id',
                                        'title' => 'Товар',
                                        'type'  => Select2::className(),
                                        'enableError' => true,
                                        'items' => Product::getAllProduct(),
                                        'options' => [
                                            'data'=> Product::getAllProduct(),

                                        ],
                                    ],   
                                    [
                                        'name'  => 'amount',
                                        'title' => 'Количество',
                                        'enableError' => true,
                                        'options' => [
                                            'style' =>'background: #fff',
                                        ]
                                    ],   
                                ]
                            ])->label('');
                        ?>
                    </div>
                </div><hr>
            </div>  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

