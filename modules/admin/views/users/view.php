<?php


use yii\widgets\DetailView;
use app\models\Users;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
?>
<div class="users-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
             'attribute'=>'company.name',
                'label'=>'Компания',
            ],
            
            'username',
            'phone',
            [
                'attribute'=>'created_at',
                'value' => function ($data) {
                    return date('d-m-Y h:i:s');
                }
            ],
            [
                'attribute'=>'updated_at',
                'value' => function ($data) {
                    return date('d-m-Y h:i:s');
                }
            ],
            'fullname',
            'post',
            [
                'attribute'=>'status',
                'value' => function ($data) {
                    return Users::getNameRole($data['status']);
                }
            ],
        ],
    ]) ?>

</div>
