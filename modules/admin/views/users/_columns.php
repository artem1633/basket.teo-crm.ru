<?php
use yii\helpers\Url;
use app\models\Users;
use app\models\Company;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'company_id',
        'filter'=> Company::getAllCompany(),
        'value' => 'company.name'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'username',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'phone',
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'fullname',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'post',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'status',
         'filter'=> Users::getRoles(),
         'value' => function ($data) {
            return Users::getNameRole($data['status']);

         }
     ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'width' => '120px',
        'viewOptions'=>['label'=>'<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>','role'=>'modal-remote'],
        'updateOptions'=>['label'=>'<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>','role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'label'=>'<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверенны?',
                          'data-confirm-message'=>'Вы действительно хотите удалить запись'], 
    ],

];   