<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */


use kartik\select2\Select2; // or kartik\select2\Select2
use app\models\Company;
use kartik\select2\Select2Asset; 
use app\models\Users;

Select2Asset::register($this); 
$data = Company::getAllCompany();
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'company_id')->widget(Select2::classname(), [
                'data'=>$data,
                'options' => ['placeholder' => 'Виберите компанию ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => 'ru'
                ],
             ]); ?> 
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
            <?= $model->isNewRecord ? $form->field($model, 'password_hash')->textInput(['maxlength' => true]) : $form->field($model, 'new_password')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>  
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'fullname')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'post')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'status')->radioList(Users::getRoles()) ?>
        </div>
        
    </div>
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-info']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
