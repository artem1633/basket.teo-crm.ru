<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class AdminController extends Controller
{
   


public function beforeAction($action)
{
    if (Yii::$app->user->isGuest) { 
        return $this->redirect(['/']);
    }
    if (Yii::$app->user->identity->status!=9) {
        return $this->redirect(['/orders/index']);    
    }
    return parent::beforeAction($action);
}

}
