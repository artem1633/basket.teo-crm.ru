<?php

namespace app\modules\admin\controllers;

use yii\web\Controller;
use app\models\Orders;
use yii;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $ordersAll= Orders::find()->count();
        $ordersSumma = Orders::find()->sum('summa');
        $ordersToday = Orders::find()
                ->where(['between', 'order_date', date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59') ])
                ->count();
        $ordersTodaySumma = Orders::find()
                ->where(['between', 'order_date', date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59') ])
                ->sum('summa');
        $orderByMounth = Yii::$app->db
                ->createCommand ('SELECT date(order_date) d, count(*) c FROM orders where order_date > "'.
                        date('Y-m-1 00:00:00').'" GROUP BY d ORDER BY d')->queryAll();
        return $this->render('index',[
            'ordersAll'=>$ordersAll,
            'ordersSumma'=>$ordersSumma,
            'ordersToday'=>$ordersToday,
            'ordersTodaySumma'=>$ordersTodaySumma,
            'orderByMounth'=>$orderByMounth,
        ]);
    }
}
