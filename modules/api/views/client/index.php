﻿<pre style="word-wrap: break-word; white-space: pre-wrap;">-------------------------------------------------------------------
-----------------API приложения------------------------------------
ключ для тестирования b9EqBtQJSQkFNdeJ8K4uicIOh2EXuoir
-------------------------------------------------------------------
Список всех компаний - GET
http://basket.teo-crm.ru/api/client/get-all-company

Принимает
access - ключ

Возвращает 
{
    "status":true,                       //
    "errors":null,                       //
    "companyList":{                      //
        'id' : ['id'],                   // id компании
        'name' :['name'],                // Название компании
        'description' :['description'],  // Описание компании
        'picture' :['description'],      // Изображение компании
        'coord_x' :['coord_x'],          // Координата X
        'coord_y' :['coord_y'],          // Координата Y
        
    }
}

-------------------------------------------------------------------
Список всех категорий компаний - GET
http://basket.teo-crm.ru/api/client/get-category-company

Принимает
access - ключ

Возвращает 
{
    "status":true,
    "errors":null,
    "categoryCompanyList":{
        'id' : ['id'],                   // id категории
        'name' :['name'],                // Название категории
    }
}

-------------------------------------------------------------------
Список всех компаний в категории - GET
http://basket.teo-crm.ru/api/client/get-company-by-category

Принимает
access - ключ
category_id - id категории

Возвращает 
{
    "status":true,
    "errors":null,
    "companyList":{                      //
        'id' : ['id'],                   // id компании
        'name' :['name'],                // Название компании
        'description' :['description'],  // Описание компании
        'picture' :['description'],      // Изображение компании
        'coord_x' :['coord_x'],          // Координата X
        'coord_y' :['coord_y'],          // Координата Y
        
    }
}

-------------------------------------------------------------------
Список всех категорий товаров - GET
http://basket.teo-crm.ru/api/client/get-category-product

Принимает
access - ключ
company_id - id компании                // если присутствует то возвращаются
                                        // только категории компании, иначе все

Возвращает 
{
    "status":true,
    "errors":null,
    "categoryProductList":{
        'id' : ['id'],                   // id категории
        'name' :['name'],                // Название категории
    }
}

-------------------------------------------------------------------
Список всех товаров - GET
http://basket.teo-crm.ru/api/client/get-all-product

Принимает
access - ключ

Возвращает 
{
    "status":true,
    "errors":null,
    "productList":{
        'id' : ['id'],                   // id компании
        'name' :['name'],                // Название товара
        'description' :['description'],  // Описание товара
        'picture' : ['picture'],         // рисунок
        'weight' :['weight'],            // вес
        'price' :['price'],              // Цена
    }
}

-------------------------------------------------------------------
Список всех товаров в категории - GET
http://basket.teo-crm.ru/api/client/get-product-by-category

Принимает
access - ключ
category_product_id - id категории
company_id  - id компании      //если указан возвращает только товар
                               //указаной компании

Возвращает 
{
    "status":true,
    "errors":null,
    "productList":{
        'id' : ['id'],                   // id компании
        'name' :['name'],                // Название товара
        'description' :['description'],  // Описание товара
        'picture' : ['picture'],         // рисунок
        'weight' :['weight'],            // вес
        'price' :['price'],              // Цена
    }
}

-------------------------------------------------------------------
Регистрирует пользователя - GET
 http://http://basket.teo-crm.ru/api/client/register

Принимает
phone - телефон
name  - имя      

Возвращает 
{
    "status":true,
    "errors":null,
    "sms":['sms'],              // СМС которое должно прийти на номер телефона (тест)
    }
}

-------------------------------------------------------------------
Подтверждение регистрации - GET
http://basket.teo-crm.ru/api/client/confirm-register

Принимает
phone - телефон
sms  - СМС подтверждения      

если нет СМС Возвращает 
{
    "status":true,
    "errors":null,
    "sms":['sms'],              // смс тест
    "sendSMS":[sendSMS]         // результат отправки тест
}
если есть СМС Возвращает 
{
    "status":true,
    "errors":null,
    "access":['access'],              // токен для доступа
    "car_type"        //марка автомобиля
    "car_model"       //модель автомобиля
    "car_number"      //номер автомобиля
    "car_color"       //цвет автомобиля
}
-------------------------------------------------------------------
Добавление заказа - GET
http://basket.teo-crm.ru/api/client/add-order

Принимает
access - ключ
company_id - id компании
default = 1 //не обязательно если присутствует сохраняются даные автомобиля по умолчанию
car_type - марка автомобиля
car_number - номер автомобиля
car_color  - цвет автомобиля
car_model - модель автомобиля (необязательное)
pay - платежные данные
"productlist": [
            {
                "product_id": 7,   // id товара
                "amount": 2        // количество
            },
            {
                "product_id": 8,
                "amount": 4
            }
        ],

Возвращает
    "status": true,
    "errors": null,
    "order": {
        "order_id":['order->id'],
        "time_max":['time_max'],
        "summa":['summa'],
    }
<i> Пример
    http://task5.litin.vn.ua/api/client/add-order?access=Ep1urZD5q95JFklc647YrnfTZegWVcFM&company_id=1&car_type=mersedes&car_number=2585&car_color=black&productlist=[{"product_id":7,"amount":2},{"product_id":43,"amount":7}]
    Возвращает
    {"status":true,"errors":null,"order":{"order_id":158,"time_max":20,"summa":3992}}    
</i>

-------------------------------------------------------------------
Статус заказа - GET
http://basket.teo-crm.ru/api/client/get-order-status

Принимает
access - ключ
order_id - id заказа
Возвращает
    "status": true,
    "errors": null,
    "order_status": ['order->status'],

Статус
            case 0: return "Предварительный";
            case 1: return "Новый";
            case 2: return "В работе";
            case 3: return "В доставке";
            case 4: return "Выполнен";

-------------------------------------------------------------------
местоположение клиента - GET
http://basket.teo-crm.ru/api/client/client-geo

Принимает
access - ключ
coord_x - координата X
coord_y - координата Y
Возвращает
    "status": true,
    "errors": null,

-------------------------------------------------------------------
Возвращает страницу "О приложении" - GET
http://basket.teo-crm.ru/api/client/about

Принимает
access - ключ
Возвращает
    "status": true,
    "errors": null,
    "about": ["about"]   // HTML страница


-------------------------------------------------------------------
Возвращает страницу "Поддержка" - GET
http://basket.teo-crm.ru/api/client/support

Принимает
access - ключ
Возвращает
    "status": true,
    "errors": null,
    "support": ["support"]   // HTML страница


-------------------------------------------------------------------
История заказов - GET
http://basket.teo-crm.ru/api/client/get-orders-history

Принимает
access - ключ
limit - количество заказов
offset - смещение от начала
Возвращает
    "status": true,
    "errors": null,
    "orders": 
    {
        "id":"165",                         //id заказа
        "company_id":"1",                   //id компании
       "order_date":"2018-12-10 15:59:39",  //Дата заказа
       "summa":"0.000",                     //Сумма заказа
       "time_max":"0",                      //Время на приготовление
       "status":"0",                        //Статус
       "car_type":"mersedes",               //Марка машины
       "car_model":null,                    //Модель машины
       "car_number":"2585",                 //Номер машины
       "car_color":"black",                 //Цвет машины
    }

-------------------------------------------------------------------
История заказов - GET
http://basket.teo-crm.ru/api/client/add-rating

Принимает
access - ключ
company_id - id компании
rate - оценка
respond - отзыв
Возвращает
    "status": true,
    "errors": null,
    
-------------------------------------------------------------------
Merchent - GET
http://basket.teo-crm.ru/api/client/get-merchant

Принимает
access - ключ
Возвращает
    'MERCHANT_PUBLIC_ID' => $param->MERCHANT_PUBLIC_ID
    'MERCHANT_API_PASS' => $param->MERCHANT_API_PASS
    


