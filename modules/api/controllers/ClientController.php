<?php

namespace app\modules\api\controllers;

use yii\rest\ActiveController;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use Yii;
use yii\web\Controller;


use app\models\Clients;
use app\models\Company;
use app\models\CategoryCompany;
use app\models\CategoryProduct;
use app\models\Product;
use app\models\Orders;
use app\models\OrdersSearch;
use app\models\OrdersItem;
use app\models\Params;
use app\models\CompanyRating;



/**
 * Default controller for the `api` module
 */
class ClientController extends Controller
{
    public $layout = false;

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['get-all-company','get-category-company','get-company-by-category', 
                    'get-category-product','get-all-product','get-all-product-company', 'get-product-by-category',
                    'client-geo','confirm-register','register','add-order','get-order-status',
                    'testpush','about','get-orders-history','support','add-rating','get-merchant'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }
    
    public function actions()
    {
        return [];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function  actionGetMerchant()
    {
        if (isset($_GET['access']))
        {
           $client = Clients::find()->where([ 'key' => $_GET['access'] ])->one();
            if($client == null) return ['status' => false, 'errors' => 'В системе нет такого ключа'];
            $param = Params::find()->one();
            return ['status' => true, 'errors' => null, 
                'MERCHANT_PUBLIC_ID' => $param->MERCHANT_PUBLIC_ID,
                'MERCHANT_API_PASS' => $param->MERCHANT_API_PASS,
                ];
        }
        else
        {
            return ['errors' => 'no key'];
        }
    }

    public function actionAddRating()
    {          
        if (isset($_GET['access']))
        {
            $client = Clients::find()->where([ 'key' => $_GET['access'] ])->one();
            if($client == null) return ['status' => false, 'errors' => 'В системе нет такого ключа'];
            if (!isset($_GET['company_id'])) return ['status' => false, 'errors' => 'Отсутствует компания'];
            if (!isset($_GET['rate'])) return ['status' => false, 'errors' => 'Отсутствует оценка'];
            
            $rating = new CompanyRating();
            $rating->company_id = $_GET['company_id'];
            $rating->rate = $_GET['rate'];
            if (isset($_GET['respond'])) {
                $rating->respond = $_GET['respond'];
            }
            $rating->save();
            return ['status' => true, 'errors' => null];
        }
        else
        {
            return ['errors' => 'no key'];
        }
    }
    
    public function actionGetAllCompany()
    {          
        if (isset($_GET['access']))
        {
            $client = Clients::find()->where([ 'key' => $_GET['access'] ])->one();
            if($client == null) return ['status' => false, 'errors' => 'В системе нет такого ключа'];
            $companyList = Company::find()->where(['online'=>'1'])->select(['id','name','description','picture','coord_x','coord_y'])
                    ->asArray()->all();
            $companys = [];
            foreach ($companyList as $id=>$company){
                $companys[$id]=$company;
                $companys[$id]['picture']='/uploads/company/'.$companys[$id]['picture'];
            }
            return ['status' => true, 'errors' => null, 'companyList'=>$companys];
        }
        else
        {
            return ['errors' => 'no key'];
        }
    }
    
    public function actionGetCategoryCompany()
    {          
        if (isset($_GET['access']))
        {
            $client = Clients::find()->where([ 'key' => $_GET['access'] ])->one();
            if($client == null) return ['status' => false, 'errors' => 'В системе нет такого ключа'];
            $categoryCompanyList = CategoryCompany::find()->asArray()->all();
            return ['status' => true, 'errors' => null, 'categoryCompanyList'=>$categoryCompanyList];
        }
        else
        {
            return ['errors' => 'no key'];
        }
    }
    
    public function actionGetCompanyByCategory()
    {          
        if (isset($_GET['access']))
        {
            $client = Clients::find()->where([ 'key' => $_GET['access'] ])->one();
            if($client == null) return ['status' => false, 'errors' => 'В системе нет такого ключа'];
            if (!isset($_GET['category_id'])) return ['status' => false, 'errors' => 'Отсутствует категория'];
            $companyList = Company::find()->where(['category_company_id'=>$_GET['category_id']])
                    ->select(['id','name','description','picture','coord_x','coord_y'])->asArray()->all();
            $companys = [];
            foreach ($companyList as $id=>$company){
                $companys[$id]=$company;
                $companys[$id]['picture']='/uploads/company/'.$companys[$id]['picture'];
            }
            return ['status' => true, 'errors' => null, 'companyList'=>$companys];
        }
        else
        {
            return ['errors' => 'no key'];
        }
    }
    
    public function actionGetCategoryProduct()
    {          
        if (isset($_GET['access']))
        {
            $client = Clients::find()->where([ 'key' => $_GET['access'] ])->one();
            if($client == null) return ['status' => false, 'errors' => 'В системе нет такого ключа'];
            //$categoryProductList = CategoryProduct::find()->asArray()->all();
            $companyId = Company::find()->where(['online'=>1])->select('id');
            //select * from product where company_id in (SELECT id from company WHERE ONLINE = 1);
            $productId = Product::find()->distinct('category_product_id')->select('category_product_id')->where(['in','company_id',$companyId]);
            $categoryProductList = CategoryProduct::find()->where(['in','id',$productId])->asArray()->all();
            return ['status' => true, 'errors' => null, 'categoryProductList'=>$categoryProductList];
        }
        else
        {
            return ['errors' => 'no key'];
        }
    }
    
    public function actionGetAllProduct()
    {          
        if (isset($_GET['access']))
        {
            $client = Clients::find()->where([ 'key' => $_GET['access'] ])->one();
            if($client == null) return ['status' => false, 'errors' => 'В системе нет такого ключа'];
            $productList = Product::find()
                    ->select(['id','name','description','picture','weight','price'])->asArray()->all();
            $products = [];
            foreach ($productList as $id=>$product){
                $products[$id]=$product;
                $products[$id]['picture']='/uploads/'.$products[$id]['picture'];
            }
            return ['status' => true, 'errors' => null, 'productList'=>$products];
        }
        else
        {
            return ['errors' => 'no key'];
        }
    }
    
    public function actionGetProductByCategory()
    {          
        if (isset($_GET['access']))
        {
            $client = Clients::find()->where([ 'key' => $_GET['access'] ])->one();
            if($client == null) return ['status' => false, 'errors' => 'В системе нет такого ключа'];
            if (!isset($_GET['category_product_id'])) return ['status' => false, 'errors' => 'Отсутствует категория'];
            if (isset($_GET['company_id'])) {
                $productList = Product::find()->where(['category_product_id'=>$_GET['category_product_id']])
                        ->andWhere(['company_id'=>$_GET['company_id']])
                    ->select(['id','name','description','picture','weight','price'])->asArray()->all();
            } else {
                $productList = Product::find()->where(['category_product_id'=>$_GET['category_product_id']])
                    ->select(['id','name','description','picture','weight','price'])->asArray()->all();    
            }
            $products = [];
            foreach ($productList as $id=>$product){
                $products[$id]=$product;
                $products[$id]['picture']='/uploads/'.$products[$id]['picture'];
            }
                
            
            return ['status' => true, 'errors' => null, 'productList'=>$products];
        }
        else
        {
            return ['errors' => 'no key'];
        }
    }

    public function actionGetAllProductCompany()
    {
        if (isset($_GET['access']))
        {
            $client = Clients::find()->where([ 'key' => $_GET['access'] ])->one();
            if($client == null) return ['status' => false, 'errors' => 'В системе нет такого ключа'];
            $productList = Product::find()->where(['company_id'=>$_GET['company_id']])
                ->select(['id','name','description','picture','weight','category_product_id','price'])->asArray()->all();
            $products = [];
            foreach ($productList as $id=>$product){
                $products[$id]=$product;
                $products[$id]['picture']='/uploads/'.$products[$id]['picture'];
            }
            return ['status' => true, 'errors' => null, 'productList'=>$products];
        }
        else
        {
            return ['errors' => 'no key'];
        }
    }
    
    public function actionRegister()
    {
        if (!isset($_GET['phone'])) return ['status' => false, 'errors' => 'Отсутствует телефон'];
        if (!isset($_GET['name'])) return ['status' => false, 'errors' => 'Отсутствует компания'];
        $client = Clients::find()->where(['phone'=>$_GET['phone']])->one();
        if ($client) {
            return ['status' => false, 'errors' => 'Телефон уже зарегистрирован'];
        }
        $client = new Clients();
        $client->key = Yii::$app->security->generateRandomString();
        $client->phone = $_GET['phone'];
        $client->name = $_GET['name'];
        $client->sms = (string) rand(0,9).(string) rand(0,9).(string) rand(0,9).(string) rand(0,9);
        $client->save();
        $sendSMS = static::requestSMS($client->phone, $client->sms);
        return ['status' => true, 'errors' => null, 
                'sms'=>$client->sms, 'sendSMS'=>$sendSMS];
        
    }
    
    public function actionConfirmRegister()
    {
        if (!isset($_GET['phone'])) return ['status' => false, 'errors' => 'Отсутствует телефон'];
        $client = Clients::find()->where(['phone'=>$_GET['phone']])->one();
        if (!$client) {
            return ['status' => false, 'errors' => 'Телефон не зарегистрирован'];
        }
        if (!isset($_GET['sms'])) {
            $client->sms = (string) rand(0,9).(string) rand(0,9).(string) rand(0,9).(string) rand(0,9);
            $client->save();
            $sendSMS = static::requestSMS($client->phone, $client->sms);
            return ['status' => true, 'errors' => null, 
                'sms'=>$client->sms, 'sendSMS'=>$sendSMS];
        }
        if ($client->sms != $_GET['sms']) {
            return ['status' => false, 'errors' => 'код SMS не верный '];
        }
        if (isset($_GET['key_push'])) {
            $client->key_push = $_GET['key_push'];
        }
        
        $client->status = 1;
        $client->save();
        return ['status' => true, 'errors' => null, 
                'access'=>$client->key,
                'car_type'=>$client->car_type,
                'car_model'=>$client->car_model,
                'car_number'=>$client->car_number,
                'car_color'=>$client->car_color,
            ];
        
    }
    
    public function actionClientGeo()
    {          
        if (isset($_GET['access']))
        {
            $client = Clients::find()->where([ 'key' => $_GET['access'] ])->one();
            if($client == null) return ['status' => false, 'errors' => 'В системе нет такого ключа'];
            if (!isset($_GET['coord_x'])) return ['status' => false, 'errors' => 'Отсутствует координата X'];
            if (!isset($_GET['coord_y'])) return ['status' => false, 'errors' => 'Отсутствует координата Y'];
            if ($_GET['coord_x']!=0) {
                $client->coord_x = $_GET['coord_x'];
            }
            if ($_GET['coord_y']!=0) {
                $client->coord_y = $_GET['coord_y'];
            }
            $client->save();
            return ['status' => true, 'errors' => null];
        }
        else
        {
            return ['errors' => 'no key'];
        }
    }


    public function actionAddOrder()
    {
        if (isset($_GET['access']))
        {
            $client = Clients::find()->where([ 'key' => $_GET['access'] ])->one();
            if($client == null) return ['status' => false, 'errors' => 'В системе нет такого ключа'];
            if (!isset($_GET['company_id'])) return ['status' => false, 'errors' => 'Отсутствует компания'];
            //Данние об автомобиле
            if (!isset($_GET['car_type'])) return ['status' => false, 'errors' => 'Отсутствует марка автомобиля'];
            if (!isset($_GET['car_number'])) return ['status' => false, 'errors' => 'Отсутствует номер автомобиля'];
            if (!isset($_GET['car_color'])) return ['status' => false, 'errors' => 'Отсутствует цвет автомобиля'];
            //сохранить по умолчанию
            if (isset($_GET['default']) && $_GET['default']==1 ){
                $client->car_type = $_GET['car_type'];
                $client->car_number = $_GET['car_number'];
                $client->car_color = $_GET['car_color'];
                if (isset($_GET['car_model'])) {
                    $client->car_model = $_GET['car_model'];
                }
                if (isset($_GET['pay'])) {
                    $client->car_model = $_GET['pay'];
                }
                $client->save();
            }
            //Создать заказ
            $order = new Orders();
            $order->company_id =$_GET['company_id'];
            $order->order_date = date("Y-m-d H:i:s");
            $order->client_id = $client->id;
            $order->status = 0;
            $order->car_type = $_GET['car_type'];
            $order->car_number = $_GET['car_number'];
            $order->car_color = $_GET['car_color'];
            if (isset($_GET['car_model'])) {
                $order->car_model = $_GET['car_model'];
            }
            if (isset($_GET['pay'])) {
                $order->car_model = $_GET['pay'];
            }
            
            $order->save();
            //Заполнить товар
            $productList = json_decode($_GET['productlist'],true);
            $maxTime = 0;
            $allSumma = 0;
            foreach ($productList as $product) {
                $item = new OrdersItem();
                $item->orders_id = $order->id;
                $item->product_id = $product['product_id'];
                $item->amount = $product['amount'];
                $productItem = Product::find()->where(['id'=>$product['product_id']])->one();
                if ($maxTime < $productItem->time_prepare) {
                    $maxTime = $productItem->time_prepare;
                }
                $item->summa = $productItem->price * $product['amount'];
                $allSumma+=$item->summa;
                $item->save();
            }
            //Заполнить суму и время
            $order->summa = $allSumma;
            $order->time_max = $maxTime;
            $order->save();
            
            return ['status' => true, 'errors' => null, 
                'order'=>['order_id' => $order->id, 'time_max' => $maxTime, 'summa' => $allSumma]];
        }
        else
        {
            return ['errors' => 'no key'];
        }
    }
    
    public function actionGetOrderStatus()
    {
       if (isset($_GET['access']))
        {
            $client = Clients::find()->where([ 'key' => $_GET['access'] ])->one();
            if($client == null) return ['status' => false, 'errors' => 'В системе нет такого ключа'];
            if (!isset($_GET['order_id'])) return ['status' => false, 'errors' => 'Отсутствует заказ']; 
            $order = Orders::find()->where(['id'=>$_GET['order_id']])->one();
            if (!$order) 
            {
               return ['status' => false, 'errors' => 'Отсутствует заказ с таким номером'];  
            }
            return ['status' => true, 'errors' => null, 
                 'order_status' => $order->status];
        }
        else
        {
            return ['errors' => 'no key'];
        }
    }

    public function actionAbout()
    {
       if (isset($_GET['access']))
        {
           $client = Clients::find()->where([ 'key' => $_GET['access'] ])->one();
            if($client == null) return ['status' => false, 'errors' => 'В системе нет такого ключа'];
            $param = Params::find()->one();
            return ['status' => true, 'errors' => null, 
                 'about' => $param->about];
        }
    }
    public function actionSupport()
    {
       if (isset($_GET['access']))
        {
           $client = Clients::find()->where([ 'key' => $_GET['access'] ])->one();
            if($client == null) return ['status' => false, 'errors' => 'В системе нет такого ключа'];
            $param = Params::find()->one();
            return ['status' => true, 'errors' => null, 
                 'support' => $param->support];
        }
    }
    
     public function actionGetOrdersHistory()
    {          
        if (isset($_GET['access']))
        {
            $client = Clients::find()->where([ 'key' => $_GET['access'] ])->one();
            if($client == null) return ['status' => false, 'errors' => 'В системе нет такого ключа'];
            $orders = Orders::find()->where(['client_id'=>$client->id])->orderBy(['id'=>SORT_DESC]);
            if (isset($_GET['limit']))  $orders->limit ($_GET['limit']);
            if (isset($_GET['offset'])) $orders->offset ($_GET['offset']);
            $orders = $orders->asArray();
            
            return ['status' => true, 'errors' => null, 'orders'=>$orders->all()];
        }
        else
        {
            return ['errors' => 'no key'];
        }
    }

        public function actionTestpush()
    {
        $url = 'https://gcm-http.googleapis.com/gcm/send';
        $message['type'] = 'order_status';
        if (!isset($_GET['status'])){ 
            $status = 0;
        } else {
            $status = $_GET['status'];
        }
        $message['order_status'] = $status;
        $url = 'https://gcm-http.googleapis.com/gcm/send';
        $fields = [
            'registration_ids' => ['eHYJmiUt97k:APA91bGrWzmErGbvJNSNAK-qDX6dg0CjBhp012s2NVTHZdhAV1rs2hUBNfsB-WnLRnzFTwU0jWG9FGAeC1-P9THad0q-d9e7Fm84JU_cEuM9yjuklMcw8B6JesuiMoFJHVndCknK4XM_'],
            'data' => ["message" => $message],
        ];

        $headers = [
            'Authorization: key=AIzaSyAkFVraWxiHJrlEStqy77uvBHBT-u58VHY',
            'Content-Type: application/json',
        ];

        // Open connection
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        // Execute post
        $result = curl_exec($ch);
        // Close connection
        curl_close($ch);
        return $result;
    }

    /**
     * Отправка СМС сообщения
     *
     * @param string $phone_number Номер телефона в формате +79999999999
     * @param string $message Текст сообщения
     * @return bool|string
     */
    public static function requestSMS($phone_number, $message)
    {
        $url = 'http://api.iqsms.ru/messages/v2/send/';
        $param = Params::find()->one();
        $params['login'] = $param->login_SMS;
        $params['password'] = $param->password_SMS;
        $params['phone'] = $phone_number;
        $params['text'] = $param->text_SMS . $message;
        $params['flash'] = 1;

        $result = file_get_contents($url, false, stream_context_create([
            'http' => [
                'request_fulluri' => true,
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            ]
        ]));

        Yii::warning($result);

        return $result;

    }
 
}
