<?php

use yii\db\Migration;

/**
 * Class m181207_080028_alter_clients_table
 */
class m181207_080028_alter_clients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('clients','car_type', $this->string(32)->comment('Марка автомобиля'));
        $this->alterColumn('clients','car_model', $this->string(32)->comment('Модель автомобиля'));
        $this->alterColumn('clients','car_number', $this->string(32)->comment('Госномер автомобиля'));
        $this->alterColumn('clients','car_color', $this->string(32)->comment('Цвет автомобиля'));
        $this->alterColumn('clients','pay', $this->text()->comment('Платежные реквизиты'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181207_080028_alter_clients_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181207_080028_alter_clients_table cannot be reverted.\n";

        return false;
    }
    */
}
