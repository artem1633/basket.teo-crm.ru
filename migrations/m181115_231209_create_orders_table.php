<?php

use yii\db\Migration;

/**
 * Handles the creation of table `orders`.
 */
class m181115_231209_create_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('orders', [
            'id' => $this->primaryKey(),
            'company_id'=> $this->integer(),
            'order_date'=> $this->dateTime()->comment('Дата заказа'),
            'summa'=> $this->decimal(8,3)->comment('Сума'),
            'coord_x'=> $this->decimal(15,8)->notNull()->comment('Координата X'),
            'coord_y'=> $this->decimal(15,8)->notNull()->comment('Координата Y'),
            'time_last'=>$this->dateTime()->comment('Время до прибытия'),
            'distance'=> $this->decimal(8,3)->comment('Расстояние до места'),
            'time_max'=> $this->integer()->comment('Время на заказ'),
            'status'=> $this->integer()->comment('Статус заказа'),
        ],$tableOptions);
        $this->createIndex('idx-orders-company_id', 'orders', 'company_id');
        $this->addForeignKey('fk-orders-company_id', 'orders', 'company_id', 'company', 'id', 'CASCADE');
   




    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('orders');
    }
}
