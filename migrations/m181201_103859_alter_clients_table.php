<?php

use yii\db\Migration;

/**
 * Class m181201_103859_alter_clients_table
 */
class m181201_103859_alter_clients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('clients','sms', $this->string(8)->null()->comment('СМС'));
        $this->addColumn('clients','status', $this->tinyInteger(1)->defaultValue(0)->comment('Статус'));
        $this->addColumn('clients','coord_x', $this->decimal(15,8)->null()->comment('Координата X'));
        $this->addColumn('clients','coord_y', $this->decimal(15,8)->null()->comment('Координата Y'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181201_103859_alter_clients_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181201_103859_alter_clients_table cannot be reverted.\n";

        return false;
    }
    */
}
