<?php

use yii\db\Migration;

/**
 * Class m181208_195417_alter_params_table
 */
class m181208_195417_alter_params_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('params','key_push', $this->string()->comment(''));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181208_195417_alter_params_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181208_195417_alter_params_table cannot be reverted.\n";

        return false;
    }
    */
}
