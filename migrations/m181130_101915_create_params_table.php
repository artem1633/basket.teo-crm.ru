<?php

use yii\db\Migration;

/**
 * Handles the creation of table `params`.
 */
class m181130_101915_create_params_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('params', [
            'id' => $this->primaryKey(),
            'login_SMS'=>$this->string()->notNull()->defaultValue('')->comment(''),
            'password_SMS'=>$this->string()->notNull()->defaultValue('')->comment(''),
            'API_SMS'=>$this->string()->notNull()->defaultValue('')->comment(''),
            'API_telegram'=>$this->string()->notNull()->defaultValue('')->comment(''),
            'proxy'=> $this->string()->notNull()->defaultValue('')->comment(''),
            'text_SMS'=>$this->string()->notNull()->defaultValue('')->comment(''),
        ], $tableOptions);
        $this->insert('params', []);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('params');
    }
}
