<?php

use yii\db\Migration;

/**
 * Class m181210_140218_alter_params_table
 */
class m181210_140218_alter_params_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('params','about', $this->text()->comment('О приложении'));
        $this->addColumn('params','support', $this->text()->comment('Поддержка'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181210_140218_alter_params_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181210_140218_alter_params_table cannot be reverted.\n";

        return false;
    }
    */
}
