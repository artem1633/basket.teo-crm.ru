<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category_product`.
 */
class m181115_230441_create_category_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('category_product', [
            'id' => $this->primaryKey(),
            'name' => $this->string(80)->notNull()->comment('Название категории'),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('category_product');
    }
}
