<?php

use yii\db\Migration;

/**
 * Handles the creation of table `clients_`.
 */
class m181115_231322_create_clients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('clients', [
            'id' => $this->primaryKey(),
            'key' => $this->string(32)->unique()->notNull(),
            'name' =>$this->string()->notNull()->comment('Имя клиента'),
            'phone' =>$this->string(12)->notNull()->comment('Телефон'),
            'car_type'=>$this->string(12)->comment('Марка автомобиля'),
            'car_model'=>$this->string(12)->comment('Модель автомобиля'),
            'car_number'=>$this->string(12)->comment('Госномер автомобиля'),
            'car_color'=>$this->string(12)->comment('Цвет автомобиля'),
            'pay'=>$this->text()->comment('Платежные реквизиты '),
                
        ]);
        $this->insert('clients',array(
            'name' => 'Клиент',
            'key' => Yii::$app->security->generateRandomString(),
            'phone' => '1234567890',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('clients');
    }
}
