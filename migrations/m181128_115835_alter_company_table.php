<?php

use yii\db\Migration;

/**
 * Class m181128_115835_alter_company_table
 */
class m181128_115835_alter_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->addColumn('company','online', $this->smallInteger(1)->defaultValue('1')->comment('Онлайн'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181128_115835_alter_company_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181128_115835_alter_company_table cannot be reverted.\n";

        return false;
    }
    */
}
