<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company`.
 */
class m181115_230542_create_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('company', [
            'id' => $this->primaryKey(),
            'name'=> $this->string(255)->notNull()->comment('Название'),
            'description'=> $this->string(3000)->notNull()->comment('Описание'),
            'picture'=> $this->string(255)->null()->comment('Картинка'),
            'category_company_id'=> $this->integer(),
            'coord_x'=> $this->decimal(8,3)->notNull()->comment('Координата X'),
            'coord_y'=> $this->decimal(8,3)->notNull()->comment('Координата Y'),
            'percent_order'=> $this->decimal(8,3)->notNull()->comment('Процент от заказа'),
        ], $tableOptions);
        $this->createIndex('idx-company-category_company_id', 'company', 'category_company_id');
        $this->addForeignKey('fk-company-category_company_id', 'company', 'category_company_id', 'category_company', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('company');
    }
}
