<?php

use yii\db\Migration;

/**
 * Class m181206_220040_alter_orders_table
 */
class m181206_220040_alter_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('orders','car_type', $this->string(32)->comment('Марка автомобиля'));
        $this->addColumn('orders','car_model', $this->string(32)->comment('Модель автомобиля'));
        $this->addColumn('orders','car_number', $this->string(32)->comment('Госномер автомобиля'));
        $this->addColumn('orders','car_color', $this->string(32)->comment('Цвет автомобиля'));
        $this->addColumn('orders','pay', $this->text()->comment('Платежные реквизиты'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181206_220040_alter_orders_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181206_220040_alter_orders_table cannot be reverted.\n";

        return false;
    }
    */
}
