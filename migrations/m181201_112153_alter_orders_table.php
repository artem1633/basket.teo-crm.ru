<?php

use yii\db\Migration;

/**
 * Class m181201_112153_alter_orders_table
 */
class m181201_112153_alter_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('orders','client_id', $this->integer()->null()->comment('Клиент'));
        $this->createIndex('idx-orders_item-client_id', 'orders', 'client_id');
        $this->addForeignKey('fk-orders_item-client_id', 'orders', 'client_id', 'clients', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181201_112153_alter_orders_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181201_112153_alter_orders_table cannot be reverted.\n";

        return false;
    }
    */
}
