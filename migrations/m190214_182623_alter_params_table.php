<?php

use yii\db\Migration;

/**
 * Class m190214_182623_alter_params_table
 */
class m190214_182623_alter_params_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('params','MERCHANT_PUBLIC_ID', $this->string()->comment('MERCHANT_PUBLIC_ID'));
        $this->addColumn('params','MERCHANT_API_PASS', $this->string()->comment('MERCHANT_API_PASS'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('params', 'MERCHANT_PUBLIC_ID');
        $this->dropColumn('params', 'MERCHANT_API_PASS');
        
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190214_182623_alter_params_table cannot be reverted.\n";

        return false;
    }
    */
}
