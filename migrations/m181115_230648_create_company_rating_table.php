<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company_rating`.
 */
class m181115_230648_create_company_rating_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('company_rating', [
            'id' => $this->primaryKey(),
            'company_id'=> $this->integer()->notNull(),
            'rate'=> $this->smallInteger(1)->comment('Оценка'),
            'respond'=> $this->string()->comment('Отзыв'),
        ], $tableOptions);
        $this->createIndex('idx-company_rating-company_id', 'company_rating', 'company_id');
        $this->addForeignKey('fk-company_rating-company_id', 'company_rating', 'company_id', 'company', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('company_rating');
    }
}
