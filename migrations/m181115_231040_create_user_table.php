<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m181115_231040_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'company_id'=> $this->integer(),
            'username' => $this->string()->notNull()->unique()->comment('Логин'),
            'password_hash' => $this->string()->notNull()->comment(''),
            'password_reset_token' => $this->string()->unique()->comment(''),
            'phone' => $this->string(12)->notNull()->unique()->comment('Телефон'),
            'created_at' => $this->integer()->comment(''),
            'updated_at' => $this->integer()->comment(''),
            'fullname' => $this->string(64)->notNull()->comment('ФИО'),
            'post'=> $this->string(32)->notNull()->comment('Должность'),
            'status' => $this->smallInteger()->notNull()->defaultValue(0)->comment('Роль'),
        ], $tableOptions);
        $this->createIndex('idx-users-company_id', 'users', 'company_id');
        $this->addForeignKey('fk-users-company_id', 'users', 'company_id', 'company', 'id', 'CASCADE');
        $this->insert('users',array(
            'fullname' => 'Иванов Иван Иванович',          
            'username' => 'admin',
            'password_hash' => Yii::$app->security->generatePasswordHash('admin'),
            'phone' => '1234567890',
            'post' => 'admin',
            'status' => '9',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
