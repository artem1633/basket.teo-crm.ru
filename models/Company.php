<?php

namespace app\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property string $name Название
 * @property string $description Описание
 * @property string $picture Картинка
 * @property int $category_company_id
 * @property string $coord_x Координата X
 * @property string $coord_y Координата Y
 * @property string $percent_order Процент от заказа
 *
 * @property CategoryCompany $categoryCompany
 */
class Company extends \yii\db\ActiveRecord
{
    
    public $files;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description', 'coord_x', 'coord_y', 'percent_order'], 'required'],
            [['category_company_id','online'], 'integer'],
            [['coord_x', 'coord_y', 'percent_order'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['picture'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 3000],
            [['category_company_id'], 'exist', 'skipOnError' => true, 'targetClass' => CategoryCompany::className(), 'targetAttribute' => ['category_company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'picture' => 'Картинка',
            'category_company_id' => 'Категория компании',
            'coord_x' => 'Координата X',
            'coord_y' => 'Координата Y',
            'percent_order' => 'Процент от заказа',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryCompany()
    {
        return $this->hasOne(CategoryCompany::className(), ['id' => 'category_company_id']);
    }
    
    public static function getAllCompany()
    {
        return ArrayHelper::map(static::find()->all(), 'id', 'name');
    }
    
    public static function getOnlineStatus()
    {
        return ArrayHelper::map(static::find()->all(), 'id', 'name');
    }

}
