<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company_rating".
 *
 * @property int $id
 * @property int $company_id
 * @property int $rate Оценка
 * @property string $respond Отзыв
 *
 * @property Company $company
 */
class CompanyRating extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_rating';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id'], 'required'],
            [['company_id', 'rate'], 'integer'],
            [['respond'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'rate' => 'Оценка',
            'respond' => 'Отзыв',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
    
    
}
