<?php

namespace app\models;

use Yii;
use app\models\OrdersItem;
use app\models\Product;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int $company_id
 * @property string $order_date
 * @property string $summa Сума
 * @property string $coord_x Координата X
 * @property string $coord_y Координата Y
 * @property int $time_last Время до прибытия
 * @property string $distance Расстояние до места
 * @property int $time_max Время на заказ
 * @property int $status
 * @property string car_type Марка автомобиля
 * @property string car_model Модель автомобиля
 * @property string car_number Госномер автомобиля
 * @property string car_color Цвет автомобиля
 * @property string pay
 *
 * @property Company $company
 * @property OrdersItem[] $ordersItems
 */
class Orders extends \yii\db\ActiveRecord
{
    public $items;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }
    
    const ORDER_PRELIMINARY = '0';
    const ORDER_NEW = '1';
    const ORDER_WORK = '2';
    const ORDER_DELIVERY = '3';
    const ORDER_COMPLETE = '4';        
    //Предварительный/Новый/В работе/В доставке/Выполнен

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id','client_id', 'time_max', 'status'], 'integer'],
            [['time_last','order_date'], 'safe'],
            [['summa', 'distance'], 'number'],
            [['car_type', 'car_model', 'car_number' , 'car_color' ],'string'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Комания',
            'order_date' => 'Дата заказа',
            'summa' => 'Сума',
            'time_last' => 'Время до прибытия',
            'distance' => 'Расстояние до места',
            'time_max' => 'Время на заказ',
            'status' => 'Статус',
            'car_type'=> 'Марка автомобиля',
            'car_model' => 'Модель автомобиля',
            'car_number' => 'Госномер автомобиля',
            'car_color'=> 'Цвет автомобиля',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersItems()
    {
        return $this->hasMany(OrdersItem::className(), ['orders_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id']);
    }
    
    /**
     * Возвращает список статусов
     * @return type
     */
    
    public static function getStatuses()
    {
        return [
            self::ORDER_PRELIMINARY => 'Предварительный',
            self::ORDER_NEW => 'Новый',
            self::ORDER_WORK => 'В работе',
            self::ORDER_DELIVERY => 'В доставке',
            self::ORDER_COMPLETE => 'Выполнен',
        ];
    }

    
    /**
     * Получить имя статуса. 
     * @param type $role
     * @return string
     */
    public static function getNameStatus($role)
    {
        switch ($role) {
            case 0: return "Предварительный";
            case 1: return "Новый";
            case 2: return "В работе";
            case 3: return "В доставке";
            case 4: return "Выполнен";
            default: return "Неизвестно";
        }
    }
    
    /**
     * Добавляет товар в заказ
     * @return type
     */
    public function addItem()
    {
        $allSumma=0;
        $maxTime=0;
        foreach ($this->items as $value) {
            $item = new OrdersItem();
            $item->orders_id= $this->id;
            $item->product_id = $value['product_id'];
            $item->amount = $value['amount'];
            $cena = Product::find()->where(['id'=>$value['product_id']])->one();
            if ($cena->time_prepare>$maxTime) {
                $maxTime=$cena->time_prepare;
            }
            $item->summa = $value['amount']*$cena->price;
            $item->save();
            $allSumma+=$value['amount']*$cena->price;
        }
        $this->summa=$allSumma;
        $this->time_max=$maxTime;
        return $this->save();
    }
    
    /**
     * Удаляет
     */
    public function deleteItem()
    {
        $items = OrdersItem::find()->where(['orders_id'=> $this->id])->all();
        foreach ($items as $value) {
            if (($rel = OrdersItem::findOne($value->id)) !== null) {
               $rel->delete();
            }
        }
    }
}
