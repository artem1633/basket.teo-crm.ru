<?php

namespace app\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "clients".
 *
 * @property int $id
 * @property string $key
 * @property string $name Имя клиента
 * @property string $phone Телефон
 * @property string $car_type Марка автомобиля
 * @property string $car_model Модель автомобиля
 * @property string $car_number Госномер автомобиля
 * @property string $car_color Цвет автомобиля
 * @property string $pay Платежные реквизиты 
 */
class Clients extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clients';
    }

    const STATUS_NOTCONFIRM = '0';
    const STATUS_CONFIRM = '1';
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key', 'name', 'phone'], 'required'],
            [['pay','push_result'], 'string'],
            [['key'], 'string', 'max' => 32],
            [['name'], 'string', 'max' => 255],
            [['phone', 'car_type', 'car_model', 'car_number', 'car_color'], 'string', 'max' => 12],
            [['key'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'name' => 'Имя клиента',
            'phone' => 'Телефон',
            'car_type' => 'Марка автомобиля',
            'car_model' => 'Модель автомобиля',
            'car_number' => 'Госномер автомобиля',
            'car_color' => 'Цвет автомобиля',
            'pay' => 'Платежные реквизиты ',
            'coord_x' => 'Координата X',
            'coord_y' => 'Координата Y',
        ];
    }
    
        //Получить список статусов.
    public static function getStatuses()
    {
        return [
            self::STATUS_NOTCONFIRM => 'Не подтвержден',
            self::STATUS_CONFIRM => 'Подтвержден',
        ];
    }

    //Получить имя статуса.
    public static function getNameStatus($status)
    {
        switch ($status) {
            case 0: return "Не подтвержден";
            case 1: return "Подтвержден";
            default: return "Неизвестно";
        }
    }
    public function getCurrentStatus()
    {
        switch ($this->status) {
            case 0: return "Не подтвержден";
            case 1: return "Подтвержден";
            default: return "Неизвестно";
        }
    }
    
     public static function getAllClients()
    {
        return ArrayHelper::map(static::find()->all(), 'id', 'name');
    }
}
