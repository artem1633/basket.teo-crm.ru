<?php

namespace app\models;
use app\models\Users;
use app\models\Company;

class User extends Users implements \yii\web\IdentityInterface
{

    


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
   public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function getCompanyPicture()
    {
        $company=Company::find()->where(['id'=> $this->company_id])->one();
        return $company->picture;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return false;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
       return \Yii::$app->security->validatePassword($password, $this->password_hash);
    }
    
    public function beforeDelete() {
        if ($this->id!=1) {
            parent::beforeDelete();
        }
    }
}
