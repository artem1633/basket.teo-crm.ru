<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property int $company_id
 * @property string $username Логин
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $phone Телефон
 * @property int $created_at
 * @property int $updated_at
 * @property string $fullname ФИО
 * @property string $post Должность
 * @property int $status Роль
 *
 * @property Company $company
 */
class Users extends ActiveRecord
{
    
    public $new_password;
    
    public function behaviors()
         {
             return [
                 'timestamp' => [
                     'class' => 'yii\behaviors\TimestampBehavior',
                     'attributes' => [
                         ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                         ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                     ],
                 ],
             ];
         }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    const ADMIN_ROOT = '9';
    const ADMIN_COMPANY = '1';
    const MANAGER_COMPANY = '0';

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'created_at', 'updated_at', 'status'], 'integer'],
            [['username', 'password_hash', 'phone', 'fullname', 'post'], 'required'],
            [['username', 'password_hash', 'password_reset_token'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 12],
            [['fullname'], 'string', 'max' => 64],
            [['post'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['phone'], 'unique'],
            [['password_reset_token'], 'unique'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'username' => 'Логин',
            'password_hash' => 'Пароль',
            'new_password' => 'Новий пароль',
            'password_reset_token' => 'Password Reset Token',
            'phone' => 'Телефон',
            'created_at' => 'Создан',
            'updated_at' => 'Изменен',
            'fullname' => 'ФИО',
            'post' => 'Должность',
            'status' => 'Роль',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
    
        public function beforeSave($insert)
        {
            if ($this->isNewRecord) {
                $this->password_hash = Yii::$app->security->generatePasswordHash($this->password_hash);
                if ($this->company_id == null) {
                    $this->company_id = Yii::$app->user->identity->company_id;
                }
            }
            if($this->new_password != null) {
                $this->password = Yii::$app->security->generatePasswordHash($this->new_password);
            }
            return parent::beforeSave($insert);
        }
        
        //Получить список ролей.
    public static function getRoles()
    {
        return [
            self::ADMIN_COMPANY => 'Администратор',
            self::MANAGER_COMPANY => 'Менеджер',
        ];
    }

    //Получить имя роли.
    public static function getNameRole($role)
    {
        switch ($role) {
            case 0: return "Менеджер";
            case 1: return "Администратор";
            case 9: return "ROOT";
            default: return "Неизвестно";
        }
    }
    public function getCurrentRole()
    {
        switch ($this->status) {
            case 0: return "Менеджер";
            case 1: return "Администратор";
            case 9: return "ROOT";
            default: return "Неизвестно";
        }
    }
}
