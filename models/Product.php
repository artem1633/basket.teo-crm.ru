<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property int $company_id
 * @property string $name Название
 * @property string $description Описание
 * @property string $weight Вес
 * @property string $picture Картинка
 * @property string $price Цена
 * @property int $time_prepare Время на приготовление
 * @property int $valid Проверенно
 * @property int $status Статус
 * @property int $category_product_id
 *
 * @property CategoryProduct $categoryProduct
 * @property Company $company
 */
class Product extends \yii\db\ActiveRecord
{
    
    public $files;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description', 'weight', 'price', 'time_prepare'], 'required'],
            [['company_id', 'time_prepare', 'valid', 'status', 'category_product_id'], 'integer'],
            [['name', 'weight', 'picture'], 'string', 'max' => 255],
            [['picture','price'], 'safe'],
            [['description'], 'string', 'max' => 3000],
            [['category_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => CategoryProduct::className(), 'targetAttribute' => ['category_product_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'name' => 'Название',
            'description' => 'Описание',
            'weight' => 'Вес',
            'picture' => 'Картинка',
            'price' => 'Цена',
            'time_prepare' => 'Время на приготовление',
            'valid' => 'Проверенно',
            'status' => 'Статус',
            'category_product_id' => 'Категория продукта',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryProduct()
    {
        return $this->hasOne(CategoryProduct::className(), ['id' => 'category_product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
    
    public function beforeSave($insert)
        {
            if ($this->isNewRecord) {
                if ($this->company_id == NULL) {
                    $this->company_id = Yii::$app->user->identity->company_id;   
                }
            }
            return parent::beforeSave($insert);
        
        }
        
    public static function getAllProduct()
    {
        return yii\helpers\ArrayHelper::map(static::find()->all(),'id','name');
    }
}