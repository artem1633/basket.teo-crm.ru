<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CompanyRating;

/**
 * CompanyRatingSearch represents the model behind the search form about `app\models\CompanyRating`.
 */
class CompanyRatingSearch extends CompanyRating
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'rate'], 'integer'],
            [['respond'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CompanyRating::find();
        if (Yii::$app->user->identity->status!=9) {
            $query ->where (['company_id'=>Yii::$app->user->identity->company_id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=>[
                'defaultOrder'=>[
                    'id'=>SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'company_id' => $this->company_id,
            'rate' => $this->rate,
        ]);

        $query->andFilterWhere(['like', 'respond', $this->respond]);

        return $dataProvider;
    }
}
