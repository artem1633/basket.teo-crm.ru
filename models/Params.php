<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "params".
 *
 * @property int $id
 * @property string $login_SMS
 * @property string $password_SMS
 * @property string $API_SMS
 * @property string $API_telegram
 * @property string $proxy
 * @property string $text_SMS
 */
class Params extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'params';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['login_SMS', 'password_SMS', 'API_SMS', 'API_telegram', 'proxy', 'text_SMS','key_push','MERCHANT_PUBLIC_ID','MERCHANT_API_PASS'], 'string', 'max' => 255],
            [['about','support'],'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login_SMS' => 'Login  Sms',
            'password_SMS' => 'Password  Sms',
            'API_SMS' => 'Api  Sms',
            'API_telegram' => 'Api Telegram',
            'proxy' => 'Proxy',
            'text_SMS' => 'Text  Sms',
            'about' => 'О приложении',
            'support' => 'Поддержка',
        ];
    }
}
