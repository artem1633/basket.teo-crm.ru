<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Users;

 $path = 'http://' . $_SERVER['SERVER_NAME'].'/uploads/company/'.Yii::$app->user->identity->companyPicture;
 //        . '/images/users/avatar.jpg';
 
?>
<?php 
        $session = Yii::$app->session;
        if($session['left'] == null | $session['left'] == 'small') $left="x-navigation-minimized";
        else $left="x-navigation-custom";
    ?>
<!-- START PAGE SIDEBAR -->
            <div class="page-sidebar page-sidebar-fixed scroll">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation <?=$left?>">
                    <li class="xn-logo">
                        <a href="<?=Url::toRoute([Yii::$app->homeUrl])?>"><?=Yii::$app->name?></a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="<?=$path?>" alt="John Doe"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="<?=$path?>" alt="John Doe"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name"><?=Yii::$app->user->identity->fullname?></div>
                                <div class="profile-data-title"><?=Yii::$app->user->identity->getCurrentRole()?></div>
                            </div>

                        </div>                                                                        
                    </li>
                    
                    <li class="xn-title">Menu</li>
                   <li <?php if (Yii::$app->controller->id=='dash-board'){ echo ' class = "active" ';}?>>
                        <?= Html::a('<span class="fa fa-desktop"></span> <span class="xn-text">Показатели</span>', ['/dash-board/index'], []); ?>
                    </li> 
                    <li <?php if (Yii::$app->controller->id=='orders'){ echo 'class="active"';}?>>
                        <?= Html::a('<span class="glyphicon glyphicon-euro"></span> <span class="xn-text">Заказы</span>', ['/orders/index'], []); ?>
                    </li> 
                    <li <?php if (Yii::$app->controller->id=='product'){ echo 'class="active"';}?>>
                        <?= Html::a('<span class="glyphicon glyphicon-shopping-cart"></span> <span class="xn-text">Товары</span>', ['/product/index'], []); ?>
                    </li> 
                    <li <?php if (Yii::$app->controller->id=='company-rating'){ echo 'class="active"';}?>>
                        <?= Html::a('<span class="glyphicon glyphicon-signal"></span> <span class="xn-text">Отзывы</span>', ['/company-rating/index'], []); ?>
                    </li> 
                    <li <?php if (Yii::$app->controller->id=='users'){ echo 'class="active"';}?>>
                        <?= Html::a('<span class="fa fa-users"></span> <span class="xn-text">Пользователи</span>', ['/users/index'], []); ?>
                    </li>
                                
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->