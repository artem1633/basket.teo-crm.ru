<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error" style="text-align: center;">

    <h1 class="text-danger"><?= Html::encode($this->title) ?></h1>
    <?php
        if($message != ''){
    ?>
        <div class="alert alert-danger">
            <?= nl2br(Html::encode($message)) ?>
        </div>
    <?php } ?>

    <div style="text-align: center;">
        Если это ошибка сервера. <br>
        Свяжитесь с тех поддержкой.
    </div>

    <div>
        <br>
        <a href="/" class="btn btn-success">На главную</a>
    </div>

</div>
