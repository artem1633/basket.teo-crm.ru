<?php

use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;
use kartik\helpers\Html;
use app\models\Company;
use app\widgets\Yandexmap;


// Массив меток
$items = [
    
    [
        'latitude' => 55.751812,
        'longitude' => 37.599292,
        'options' => [
            [
                'hintContent' => 'Подсказка при наведении на маркет',
                'balloonContentHeader' => 'Заголовок после нажатия на маркер',
                'balloonContentBody' => 'Контент после нажатия на маркер',
                'balloonContentFooter' => 'Футер после нажатия на маркер',
            ],
            [
                'preset' => 'islands#circleIcon',
                'iconColor' => '#19aa8d',
                'draggable' => true
            ]
        ]
    ],
    
    
];
// вывод карты


/* @var $this yii\web\View */
/* @var $model app\models\Orders */
?>
    <style>
        html, body, #map {
            width: 100%;
            height: 100%;
            padding: 0;
            margin: 0;
        }
    </style>
<div class="orders-view">
    
    <div class="box box-default" style="margin-top: 30px;margin-bottom: 20px">
    <div class="box-body">
    
       <div class="row">
                <div id="map">
                </div>
           <div class="col-md-6">
        <table style="border-style: solid;margin-left: 20px">
            <tr>
            <td>
            <div class="col-md-12">
                <div class="table-responsive">
                    <table style="margin-top: 20px;margin-left: 20px;font-size: 14px">
                        <tbody>
                            <tr>
                                <td style="width: 280px"><?=$model->getAttributeLabel('company_id')?></td>
                                <td ><b><?=Html::encode($model->company->name)?></b></td>
                            </tr>
                            <tr>
                                <td style="width: 280px"><?=$model->getAttributeLabel('order_date')?></td>
                                <td ><b><?=Html::encode($model->order_date)?></b></td>
                            </tr>
                        </tbody>
                    </table><hr>
    
                    <table style="margin-top: 20px;margin-left: 20px;font-size: 14px">
                        <tbody>
                            <tr>
                                <td style="width: 280px"><?=$model->getAttributeLabel('summa')?></td>
                                <td ><b><?=Html::encode($model->summa)?></b></td>
                            </tr>
                            <tr>
                                <td style="width: 280px"><?=$model->getAttributeLabel('time_last')?></td>
                                <td ><b><?=Html::encode($model->time_last)?></b></td>
                            </tr>
                            <tr>
                                <td style="width: 280px"><?=$model->getAttributeLabel('time_max')?></td>
                                <td ><b><?=Html::encode($model->time_max)?></b></td>
                            </tr>
                        </tbody>
                    </table><hr>
                    <h3 style="color: #1b78b6"><i class="fa fa-user"> </i> <b> Личная информация</b></h3>
                    <table style="margin-top: 20px;margin-left: 20px;font-size: 14px">
                        <tbody>
                            <tr>
                                <td style="width: 280px"><?=$model->getAttributeLabel('car_type')?></td>
                                <td ><b><?=Html::encode($model->car_type)?></b></td>
                            </tr>
                            <tr>
                                <td style="width: 280px"><?=$model->getAttributeLabel('car_model')?></td>
                                <td ><b><?=Html::encode($model->car_model)?></b></td>
                            </tr>
                            <tr>
                                <td style="width: 280px"><?=$model->getAttributeLabel('car_number')?></td>
                                <td ><b><?=Html::encode($model->car_number)?></b></td>
                            </tr>
                            <tr>
                                <td style="width: 280px"><?=$model->getAttributeLabel('car_color')?></td>
                                <td ><b><?=Html::encode($model->car_model)?></b></td>
                            </tr>
                        </tbody>
                    </table><hr>
                    <h3 style="color: #1b78b6"><i class="glyphicon glyphicon-ice-lolly-tasted"> </i> <b> Информация о товаре</b></h3>
                    <table class="table-bordered" style="margin-top: 20px;margin-left: 20px;font-size: 14px">
                        <thead>
                            <tr>
                                <th style="width: 60%">Товар</th>
                                <th style="width: 20%">К-во</th>
                                <th style="width: 20%">Сумма</th>        
                            </tr>
                        </thead>
                        
                        <tbody>
                            <?php foreach ($model->ordersItems as $item) :?>
                            <tr>
                                
                                <td ><?=Html::encode($item->product->name)?></td>
                                <td ><?=Html::encode($item->amount)?></td>
                                <td ><?=Html::encode($item->summa)?></td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>

                    <br>
                </div>
            </div>
            </td>
            </tr>
            </table>
       </div>
           

           
       </div>        
    </div>
</div>
</div>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=&lt;ваш API-ключ&gt;" type="text/javascript"></script>
	<style>
         #map {
            width: 50%; height: 50%; padding: 0; margin: 0;float: left;
        }
    </style>

<script>
function init () {
    /**
     * Создаем мультимаршрут.
     * Первым аргументом передаем модель либо объект описания модели.
     * Вторым аргументом передаем опции отображения мультимаршрута.
     * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/multiRouter.MultiRoute.xml
     * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/multiRouter.MultiRouteModel.xml
     */
    var multiRoute = new ymaps.multiRouter.MultiRoute({
        // Описание опорных точек мультимаршрута.
        referencePoints: [
            [55.734876, 37.59308],
            "Москва, ул. Мясницкая"
        ],
        // Параметры маршрутизации.
        params: {
            // Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
            results: 2
        }
    }, {
        // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
        boundsAutoApply: true
    });

    

    // Создаем карту с добавленными на нее кнопками.
    var myMap = new ymaps.Map('map', {
        center: [55.750625, 37.626],
        zoom: 7,
    }, {
        buttonMaxWidth: 300
    });

    // Добавляем мультимаршрут на карту.
    myMap.geoObjects.add(multiRoute);
}

ymaps.ready(init);
</script>
