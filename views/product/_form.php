<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use app\models\CategoryProduct;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>   
                <?= $form->field($model, 'category_product_id')->dropDownList(CategoryProduct::getAllCategoryProduct())?>
                <div class="col-md-6">
                     <?= $form->field($model, 'weight')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
                </div>   
            <?= $form->field($model, 'time_prepare')->textInput() ?>
        <div class="col-md-6">
            <?= $form->field($model, 'status')->checkbox() ?>
        </div>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'files')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
                'pluginOptions' => [
                    'initialPreview'=>'/uploads/'.$model->picture,
                    'initialPreviewAsData'=>true,
                    'overwriteInitial'=>true,
                    'showPreview' => true,
                    'showCaption' => true,
                    'showRemove' => true,
                    'showUpload' => false,
                    'browseLabel' => '',
                    'removeLabel' => '',
                ]
            ]); ?>
        </div>
    </div>
    <div class="row">
        

        

        
    </div>
     


   



    

    



  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
