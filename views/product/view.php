<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
?>
<div class="product-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'description',
            'weight',
            'price',
            'time_prepare:datetime',
            [
                'attribute'=>'valid',
                'format'=>"raw",
                'value'=>function ($data) {
                return $data->valid ? '<span style="color:green" class = "glyphicon glyphicon-ok"></span>' :'<span style="color:red" class = "glyphicon glyphicon-remove"></span>';
                }
            ],
            [
                'attribute'=>'status',
                'format'=>"raw",
                'value'=>function ($data) {
                    return $data->status ? '<span style="color:green" class = "glyphicon glyphicon-ok"></span>' :'<span style="color:red" class = "glyphicon glyphicon-remove"></span>';
                }
            ],
        ],
    ]) ?>
    <div class="row">
        <div class="col-md-12">
    <?php echo '<img src="/uploads/'.$model->picture.'" width="200px" height="auto">'; ?>
        </div>
    </div>
    

</div>
