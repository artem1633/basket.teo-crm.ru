<?php
use yii\helpers\Url;
use app\helpers\StatusIcon;
use app\models\CategoryProduct;


return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'description',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'category_product_id',
        'filter'=> CategoryProduct::getAllCategoryProduct(),
        'value'=>'categoryProduct.name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'weight',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'picture',
        'format' => 'raw',
        'value' => function ($model) {   
            if ($model->picture!='')
                return '<img src="/uploads/'.$model->picture.'" width="50px" height="auto">'; else return 'no image';
            },
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'price',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'time_prepare',
     ],
[
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'valid',
         
         'format'=>"raw",
         'filter'=> StatusIcon::StatusList(),
         'value'=>function ($data) {
            return StatusIcon::StatusIcon($data->valid);
         }
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'status',
        'format'=>"raw",
         'filter'=> StatusIcon::StatusList(),
         'value'=>function ($data) {
            return StatusIcon::StatusIcon($data->status);
         }
     ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'width' => '120px',
        'viewOptions'=>['label'=>'<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>','role'=>'modal-remote'],
        'updateOptions'=>['label'=>'<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>','role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'label'=>'<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Вы уверенны?',
                          'data-confirm-message'=>'Вы действительно хотите удалить запись'], 
    ],

];   