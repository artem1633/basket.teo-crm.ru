<?php
use yii\helpers\Url;
use app\helpers\Rating;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'rate',
        'format'=>'raw',
        'value'=> function($data) {
            return Rating::Rate($data->rate);
        }
    ,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'respond',
    ],
    

];   