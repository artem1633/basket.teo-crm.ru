<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CompanyRating */

?>
<div class="company-rating-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
